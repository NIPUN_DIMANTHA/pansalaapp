package com.pansala;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

public class adminDashboard extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private CardView monkCard, userCard, templeCard, adminCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);
        toolbar = findViewById(R.id.admin_action_bar);
        setSupportActionBar(toolbar);

        monkCard = (CardView) findViewById(R.id.monk_list);
        userCard = (CardView) findViewById(R.id.user_list);
        templeCard = (CardView) findViewById(R.id.temple_list);
        adminCard = (CardView) findViewById(R.id.admin_list);

        monkCard.setOnClickListener(this);
        userCard.setOnClickListener(this);
        templeCard.setOnClickListener(this);
        adminCard.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()){
            case R.id.monk_list : i = new Intent(this, MonkList.class ); startActivity(i); break;
            case R.id.user_list : i = new Intent(this, UserList.class ); startActivity(i); break;
            case R.id.temple_list : i = new Intent(this, TempleList.class ); startActivity(i); break;
            case R.id.admin_list : i = new Intent(this, AdminList.class ); startActivity(i); break;
            default:break;


        }
    }
}
